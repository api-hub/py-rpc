import pytest
import json

import sys
sys.path.append('../')

from pyrpc.aws_lambda import FunctionFactory as func

# Update domain, region and stage setting
func.DOMAIN_NAME = 'rpc.local'
func.DEFAULT_REGION_NAME = 'ap-northeast-1'
func.STAGE = 'at-ut'

func = func.init()

@pytest.fixture(scope='session')
def method_a():
    @func.define(parameter=['app_id', 'context', 'custom'])
    @func.as_json()
    def handle_method_a(data):
        return data
    return handle_method_a
