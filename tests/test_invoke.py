def test_run_method_a(method_a):
    import traceback
    import json
    try:
        print(method_a(
            app_id='debug',
            context={
                'platform_id': '<platform_id>', 'user_id': '<user_id>'
            },
            custom={
                # this is empty custom data field
            }
        ))
    except Exception as err:
        if str(err) != 'error/unable-to-find-record':
            raise
