import boto3
import dns.resolver
import json

from cachetools import cached, TTLCache
from functools import wraps
from threading import RLock


class FunctionFactory:
    """Factory model for generating lambda handling"""
    DOMAIN_NAME = 'rpc.internal'
    DEFAULT_REGION_NAME = 'ap-northeast-1'
    STAGE = 'at-ut'
    CACHE_MAX_SIZE = 10000
    CACHE_TTL = 30

    class Map(dict):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)

        def __getattr__(self, attr):
            return self.get(attr)

    @staticmethod
    def init(lock=RLock()):
        @cached(cache=TTLCache(maxsize=FunctionFactory.CACHE_MAX_SIZE, ttl=FunctionFactory.CACHE_TTL), lock=lock)
        def lookup(app_id, method_name, ver='latest'):
            """Perform SRV lookup on service record by configuration"""
            fqdn = f'_{app_id}_{FunctionFactory.STAGE}._{method_name}.{FunctionFactory.DOMAIN_NAME}'
            try:
                records = dns.resolver.query(fqdn, 'SRV')
            except dns.resolver.NXDOMAIN:
                raise ValueError('error/unable-to-find-record')
            for r in records:
                return str(r.target).rstrip('.')
            else:
                raise ValueError('error/unable-to-find-record')

        def define(parameter=[], region_name='', invoke_type='RequestResponse'):
            if not region_name:
                region_name = FunctionFactory.DEFAULT_REGION_NAME
            client = boto3.client('lambda', region_name=region_name)

            def func_decorator(func):
                @wraps(func)
                def func_decoratored(**payload_as_dict):
                    try:
                        payload = dict((k, payload_as_dict[k]) for k in parameter)
                        response = client.invoke(
                            FunctionName=lookup(
                                app_id=payload['app_id'],
                                method_name=func.__name__
                            ),
                            InvocationType=invoke_type,
                            Payload=bytes(
                                json.dumps(payload), 'utf-8'
                            )
                        )
                        response = response['Payload'].read()
                    except Exception as e:
                        return func(e, None)
                    else:
                        return func(None, response)
                return func_decoratored
            return func_decorator

        def as_byte():
            def func_decorator(func):
                @wraps(func)
                def func_decoratored(err, response):
                    if err is not None:
                        raise err
                    return func(response)
                return func_decoratored
            return func_decorator

        def as_str():
            def func_decorator(func):
                @wraps(func)
                def func_decoratored(err, response):
                    if err is not None:
                        raise err
                    return func(response.decode('utf-8'))
                return func_decoratored
            return func_decorator

        def as_json(unpack=False):
            def func_decorator(func):
                @wraps(func)
                def func_decoratored(err, response):
                    if err is not None:
                        raise err
                    response = json.loads(response)
                    if unpack is True:
                        return func(**response)
                    else:
                        return func(response)
                return func_decoratored
            return func_decorator

        return FunctionFactory.Map(
            lock=lock,
            lookup=lookup,
            define=define,
            as_byte=as_byte, as_str=as_str, as_json=as_json
        )
