# py-rpc
RPC definition via Lambda Application SDK

## 如何使用 pyrpc?

```python
# Module
from pyrpc.aws_lambda import FunctionFactory as func

func.DOMAIN_NAME = 'oopocket.com'
func.DEFAULT_REGION_NAME = 'ap-northeast-1'
func.STAGE = 'at-ut'

@func.define(parameter=['app_id', 'arg1', 'arg2'])
@func.as_json(unpack=True)
def handle_method_a(field_1st, field_2nd, field_3rd):
    # Response data will be unpacked into key word fields if unpack is True
    return {
        'result': f'{field_1st} - {field_2nd} - {field_3rd}'
    }

# Script
from module import handle_method_a

response_as_json = handle_method_a(
    app_id='debug',
    arg1='value-for-arg1',
    arg2='value-for-arg2'
)

print(response_as_json)
# Output {'result':'<field_1st> - <field_2nd> - <field_3rd>'}
```
