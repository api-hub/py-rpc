import os
import json
from setuptools import setup

f = open(os.path.join(os.path.dirname(__file__), 'README.md'))
readme = f.read()
f.close()

setup(
    name='py-rpc',
    version='1.0.0',
    description='RPC definition vai Lambda Application SDK',
    long_description=readme,
    author='OOPocket',
    author_email='thirdpartydxt@gmail.com',
    url='',
    packages=[
        'pyrpc'
    ],
    include_package_data=True,
    install_requires=[
        'boto3>=1.9.0',
        'cachetools>=3.1.0',
        'dnspython>=1.16.0'
    ],
    license="MIT",
    zip_safe=False,
    classifiers=[
        'Programming Language :: Python :: 3.6',
    ],
)
